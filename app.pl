#!/usr/bin/env perl

use Mojolicious::Lite;

use lib 'lib';
use App::Model::Users;

# Make signed cookies tamper resistant
app->secrets(['Mojolicious rocks']);

# Helper to lazy initialize and store our model object
helper users => sub {
  state $users = MyApp::Model::Users->new;
};


# landing page
get '/' => sub {
  my $c = shift;
  $c->render(template => 'index');
};


# /login/?user=sebastian&pass=secr3t
any '/login' => sub {
  my $c = shift;
 
  # Query parameters
  my $user = $c->param('user') // '';
  my $pass = $c->param('pass') // '';
 
  # Check password
  if ($c->users->check($user, $pass)) {
    # Store username in session
    $c->session(user => $user);
    
    # Store a friendly message for the next page in flash
    $c->flash(message => 'Thanks for logging in.');
    
    # Redirect to protected page with a 302 response
    $c->redirect_to('protected');
    
  } else {
    # Failed
    $c->render(text => 'Wrong username or password.');
  }
};


# Make sure user is logged in for actions in this group
group {
  under sub {
    my $c = shift;
    
    # Redirect to main page with a 302 response if user is not logged in
    return 1 if $c->session('user');
    $c->redirect_to('/');
    return undef;
  };
  
  # A protected page auto rendering "protected.html.ep"
  get '/protected';
};


# Logout action
get '/logout' => sub {
  my $c = shift;
 
  # Expire and in turn clear session automatically
  $c->session(expires => 1);
 
  # Redirect to main page with a 302 response
  $c->redirect_to('/');
};


app->start;
