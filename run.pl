#!/usr/bin/env perl

use warnings;
use strict;
use v5.10;
use FindBin qw/$Bin/;

chdir $Bin;

if ($ENV{GIMME_SHELL}) {
  say "Dropping to shell";
  exec qw{/bin/bash -li};
}

if ( -x "/usr/share/elasticsearch/bin/elasticsearch") {
  say "Running elasticsearch";
  system("sudo -u elasticsearch -g elasticsearch /usr/share/elasticsearch/bin/elasticsearch &");
}

if ($Bin eq "/app") {
  say "Running production server";
  exec qw{carton exec hypnotoad -f ./app.pl};
} else {
  say "Running development server";
  exec qw{carton exec morbo ./app.pl};
}

