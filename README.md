# Mojo Test

Playing around with Mojolicious!

First cut based on https://metacpan.org/pod/distribution/Mojolicious/lib/Mojolicious/Guides/Growing.pod

# Run (dev)

    carton exec morbo ./app.pl

or

    ./run.pl

# Test

    prove

or

    ./app.pl test

# Docker

## Build

    docker build -t mojo-test-image .

## Cleanup shit

Delete untagged images

    docker rmi $(docker images -q -f dangling=true)

Cleanup any leftover containers (if you have build errors)

    docker rm $(docker ps -a -q)

## Run

    docker run -it --rm -p 8080:8080 mojo-test-image mojo-test-container

Note -p has to come before the image name, can't just put these option flags anywhere, this is docker and it is built by people who use MS-DOS batch files.

use `-e GIMME_SHELL=1` to run bash instead.
