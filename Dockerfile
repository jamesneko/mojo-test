FROM debian:stable
MAINTAINER James Clark <james@lazycat.com.au>

# Initial tools to get repositories set up
RUN apt-get update && apt-get upgrade -y && apt-get install -y apt-transport-https sudo wget gnupg

# Add Elasticsearch repository
WORKDIR /tmp
RUN wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
RUN echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list

RUN apt-get update

# Install carton and elasticsearch
RUN apt-get install -y carton openjdk-8-jre elasticsearch


# Docker is a piece of shit and if you were to run
#   COPY app.pl cpanfile cpanfile.snapshot templates ./
# You would find that app.pl, the cpanfiles, and the CONTENTS OF templates/ are now in the /app dir.
# What A Piece Of Shit. This doesn't even count as BSD cp semantics nonsense, because there's no
# trailing '/' on 'templates'. It just decides to empty the contents of the directory all over the
# floor because it's a stupid tool written by people who just don't get it.
# https://github.com/moby/moby/issues/15858
COPY . /app
WORKDIR /app
RUN find .
RUN carton install --deployment

# Hypnotoad defaults to 8080, Morbo defaults to 3000.
EXPOSE 8080

ENTRYPOINT ["/app/run.pl"]
